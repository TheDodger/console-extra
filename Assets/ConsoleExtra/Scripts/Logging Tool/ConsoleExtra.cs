﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ConsoleExtra : MonoBehaviour
{
#if UNITY_EDITOR
    public static void Log(string lMessage, GameObject lGameObject, ConsoleExtraEnum.EDebugType lEDebugType)
    {
        LoggingExtra.Log(lMessage, lGameObject, lEDebugType);
    }

#else
    public static void Log(string lMessage, GameObject lGameObject, ConsoleExtraEnum.EDebugType lEDebugType)
    {
        List<string> lNames = Enum.GetNames(typeof(ConsoleExtraEnum.LogError)).ToList();
        if(lNames.Contains(lEDebugType.ToString()) == true)
        {
            Debug.Log(lEDebugType.ToString() + " **************** " + lGameObject.name + "  " + lMessage);
        }
    }
#endif
}
