namespace ConsoleExtraEnum
{
  public enum EDebugTypeBase
  {
      All,
      Audio,
      Init,
      InitAndFixed,
      Contact,
      Rotation,
      Disabled,
      Destroyed,
      Finish,
      Test123Test,
  }



  public enum EDebugType
  {
      Init = EDebugTypeBase.Init,
      InitAndFixed = EDebugTypeBase.InitAndFixed,
      Contact = EDebugTypeBase.Contact,
      Rotation = EDebugTypeBase.Rotation,
      Disabled = EDebugTypeBase.Disabled,
      Destroyed = EDebugTypeBase.Destroyed,
      Finish = EDebugTypeBase.Finish,
      Test123Test = EDebugTypeBase.Test123Test,
  }



  public enum LogError
  {
      Init = EDebugTypeBase.Init,
      InitAndFixed = EDebugTypeBase.InitAndFixed,
      Contact = EDebugTypeBase.Contact,
      Rotation = EDebugTypeBase.Rotation,
      Disabled = EDebugTypeBase.Disabled,
      Destroyed = EDebugTypeBase.Destroyed,
      Finish = EDebugTypeBase.Finish,
  }



  public enum EDebugSpecailList
  {
      All = EDebugTypeBase.All,
      Audio = EDebugTypeBase.Audio,
  }
}
