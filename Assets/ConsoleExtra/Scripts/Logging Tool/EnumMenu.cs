﻿
using UnityEngine;

using UnityEditor;

using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

// Simple script that creates a new non-dockable window


public class EnumMenu
 : EditorWindow
{

    //--------------------------------------------
    // These will be shown in "Console Extra"
    //--------------------------------------------





    //---------------------------------------------------------------
    // these are the enums that you can use when using ConsoleExtra.Log()
    //---------------------------------------------------------------
    public enum ESearchType
    {
        Message,
        CallStack,
        GameObjectName,
        All,
    }


    const string m_FakeControlID = "Fake";


    public static EnumMenu m_Window = null;

    static public Consts m_Consts = null;
    static bool m_LoadVariablesOnStart = false;





    [System.Serializable]
    public class Consts
    {
        public List<string> EDebugTypeBaseNameStrings;

        public Dictionary<ConsoleExtraEnum.EDebugTypeBase, bool> UserList = new Dictionary<ConsoleExtraEnum.EDebugTypeBase, bool>();
        public Dictionary<ConsoleExtraEnum.EDebugTypeBase, bool> ErrorList = new Dictionary<ConsoleExtraEnum.EDebugTypeBase, bool>();


        public int ErrorListFlag = int.MaxValue;

        public int UserListFlag = int.MaxValue;
        public Vector2 m_ScrollPos = Vector2.zero;
    }




    //--------------------------------------------------
    // these are what apear in the console its self
    //--------------------------------------------------


    //-------------------------------------------------------------------
    // when in #if !UNITY_EDITOR mode then it will log only these enums
    //-------------------------------------------------------------------





    Logging m_Logging;
    Settings m_Settings;
    public EnumMenu(Logging lLogging, Settings lSettings)
    {
        m_Window = (EnumMenu)EditorWindow.GetWindow(typeof(EnumMenu), true, "EnumMenu");
        m_Window.maxSize = new Vector2(215f * 2.5f, 110f * 4);
        m_Window.minSize = m_Window.maxSize;
        m_LoadVariablesOnStart = false;
        m_Logging = lLogging;
        m_Settings = lSettings;
        Load();
    }


    /// <summary>
    /// 
    /// </summary>   
    EnumMenu()
    {
        m_Consts = new Consts();
        m_LoadVariablesOnStart = false;
    }

    /// <summary>
    /// 
    /// </summary>
    string CommentString(bool lWriteComment)
    {
        if (lWriteComment == true)
        {
            return @"//";
        }
        return "";
    }

    /// <summary>
    /// 
    /// </summary>
    bool GetFileName(ref string lFile_CS_BuildFileName, ref string lFileEndDistention, ref string lFile_CSC, ref EType lType)
    {
        string lTrunkPath = Directory.GetCurrentDirectory();
        string[] filePathsCS = Directory.GetFiles(lTrunkPath, "ConsoleExtraEnum.cs", SearchOption.AllDirectories);
        string[] filePathsDLL = Directory.GetFiles(lTrunkPath, "ConsoleExtraEnum.dll", SearchOption.AllDirectories);

        string lRealPath = "";
        if (filePathsDLL.Length == 1)
        {
            lType = EType.DLL;
            lRealPath = filePathsDLL[0];
        }
        else if (filePathsCS.Length == 1)
        {
            lType = EType.CS;
            lRealPath = filePathsCS[0];
        }
        else
        {
            string lErrorMessage = "";
            if (filePathsDLL.Length > 1)
            {
                foreach (string lString in filePathsDLL)
                {
                    lErrorMessage += lString + "\n";
                }
                EditorUtility.DisplayDialog("To Many DLL's", lErrorMessage, "OK");
            }
            else if (filePathsCS.Length > 1)
            {
                foreach (string lString in filePathsCS)
                {
                    lErrorMessage += lString + "\n";
                }
                EditorUtility.DisplayDialog("To Many CS's", lErrorMessage, "OK");
            }
            else
            {
                EditorUtility.DisplayDialog("No ConsoleExtraEnum.cs, or ConsoleExtraEnum.dll", lErrorMessage, "OK");
            }
            lType = EType.INVALID;
            return false;
        }

        string lItemDirectory = Path.GetDirectoryName(lRealPath);

        lFile_CS_BuildFileName = lTrunkPath += @"\ConsoleExtraEnum.cs";
        lFileEndDistention = lRealPath;



        if (lType == EType.DLL && File.Exists(m_Logging.m_Consts.CSC_Directory) == false)
        {
            EditorUtility.DisplayDialog("Missing csc.exe file  place in:", lFile_CSC, "OK");
            return false;
        }

        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    enum EType
    {
        CS,
        DLL,
        INVALID,
    }

    /// <summary>
    /// 
    /// </summary>
    void Write_CS_File(StreamWriter sw, FileStream fs)
    {
        List<string> lAll = Enum.GetNames(typeof(ConsoleExtraEnum.EDebugTypeBase)).OfType<string>().ToList();
        List<string> lSpecail = Enum.GetNames(typeof(ConsoleExtraEnum.EDebugSpecailList)).OfType<string>().ToList();
        lAll.RemoveAll(item => lSpecail.Contains(item));

        sw.WriteLine("namespace ConsoleExtraEnum");
        sw.WriteLine("{");

        sw.WriteLine("  public enum EDebugTypeBase");
        sw.WriteLine("  {");

        foreach (var lString in Enum.GetNames(typeof(ConsoleExtraEnum.EDebugSpecailList)).OfType<string>().ToList())
        {
            sw.WriteLine("      " + lString + ",");
        }
        for (int i = 0; i < m_Consts.EDebugTypeBaseNameStrings.Count; i++)
        {
            sw.WriteLine("      " + m_Consts.EDebugTypeBaseNameStrings[i] + ",");
        }



        sw.WriteLine("  }");

        sw.WriteLine("");
        sw.WriteLine("");
        sw.WriteLine("");

        sw.WriteLine("  public enum EDebugType");
        sw.WriteLine("  {");
        foreach (string lBase in m_Consts.EDebugTypeBaseNameStrings)
        {
            sw.WriteLine("      " + lBase + " = EDebugTypeBase." + lBase + ",");
        }
        sw.WriteLine("  }");



        sw.WriteLine("");
        sw.WriteLine("");
        sw.WriteLine("");

        sw.WriteLine("  public enum LogError");
        sw.WriteLine("  {");
        foreach (var lItem in m_Consts.ErrorList)
        {
            if (m_Consts.EDebugTypeBaseNameStrings.Contains(lItem.Key.ToString()) == true)
            {
                string lString = lItem.Key.ToString();
                sw.WriteLine("      " + CommentString(!lItem.Value) + lString + " = EDebugTypeBase." + lString + ",");
            }
        }
        sw.WriteLine("  }");

        sw.WriteLine("");
        sw.WriteLine("");
        sw.WriteLine("");


        sw.WriteLine("  public enum EDebugSpecailList");
        sw.WriteLine("  {");

        foreach (var lString in Enum.GetNames(typeof(ConsoleExtraEnum.EDebugSpecailList)).OfType<string>().ToList())
        {
            sw.WriteLine("      " + lString + " = EDebugTypeBase." + lString + ",");
        }
        sw.WriteLine("  }");


        sw.WriteLine("}");

        sw.Close();
        fs.Close();
    }

    /// <summary>
    /// 
    /// </summary>
    bool Save()
    {
        EType lType = EType.CS;
        string lFile_CS_BuildFileName = "";
        string lFileEndDistention = "";
        string lFile_CSC = "";
        bool lIsvalidFilename = GetFileName(ref lFile_CS_BuildFileName, ref lFileEndDistention, ref lFile_CSC, ref lType);

        if (lIsvalidFilename == false)
        {
            return false;
        }


        FileStream fs;
        if (File.Exists(lFile_CS_BuildFileName) == true)
        {
            fs = new FileStream(lFile_CS_BuildFileName, FileMode.Truncate, FileAccess.Write);

        }
        else
        {
            fs = new FileStream(lFile_CS_BuildFileName, FileMode.CreateNew, FileAccess.Write);
        }


        StreamWriter sw = new StreamWriter(fs);
        Write_CS_File(sw, fs);


        System.Threading.Thread.Sleep(2000);


        if (lType == EType.DLL)
        {
            File.Delete(lFileEndDistention);
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = lFile_CSC;
            startInfo.Arguments = @"/target:library /out:" + lFileEndDistention + " " + lFile_CS_BuildFileName;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;

            Process.Start(startInfo);

            System.Threading.Thread.Sleep(100);
            File.Delete(lFile_CS_BuildFileName);
        }
        else
        {
            File.Copy(lFile_CS_BuildFileName, lFileEndDistention, true);
            File.Delete(lFile_CS_BuildFileName);
        }

        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    bool IsFileClosed(string filename)
    {
        try
        {
            using (var inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                return true;
            }
        }
        catch (IOException)
        {
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void Load()
    {
        m_Consts = new Consts();
        m_Consts.EDebugTypeBaseNameStrings = Enum.GetNames(typeof(ConsoleExtraEnum.EDebugTypeBase)).OfType<string>().ToList();
        foreach (var lString in Enum.GetNames(typeof(ConsoleExtraEnum.EDebugSpecailList)).OfType<string>().ToList())
        {
            m_Consts.EDebugTypeBaseNameStrings.Remove(lString);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void OnGUI()
    {
        if (m_Logging == null)
        {
            this.Close();
        }
        if (m_Consts != null)
        {
            OnInspectorUpdate();
            if (m_LoadVariablesOnStart == true)
            {
                FillInWindowSetting();
            }
        }
        Repaint();
        Logging.m_Repaint = true;
    }

    /// <summary>
    /// 
    /// </summary>
    void OnInspectorUpdate()
    {
        if (m_Logging == null)
        {
            this.Close();
        }
        if (m_Window == null)
        {
            m_Window = (EnumMenu)EditorWindow.GetWindow(typeof(EnumMenu), false, "EnumMenu");
        }

        if (m_LoadVariablesOnStart == false)
        {
            Load();
            m_LoadVariablesOnStart = true;
        }
        Repaint();
        Logging.m_Repaint = true;
    }





    /// <summary>
    /// 
    /// </summary>
    void FillInWindowSetting()
    {
        if (m_Logging == null)
        {
            this.Close();
        }
        GUI.backgroundColor = Color.white;
        m_Consts.m_ScrollPos = EditorGUILayout.BeginScrollView(m_Consts.m_ScrollPos, true, true);

        DisplayList("EDebugTypeBase", ref m_Consts.EDebugTypeBaseNameStrings);



        const int lSpaceAtBeginning = 120;
        const int lNameWidth        = 120;
        const int lInformationWidth = 120;
        const int lNameHieght       = 20;
        Helper.EditorGUILayout.EditorGUILayoutInfo lInfo1 = new Helper.EditorGUILayout.EditorGUILayoutInfo(1, lSpaceAtBeginning * 0, lNameWidth, lInformationWidth, lNameHieght);

       //HACK m_Consts.UserList  = Helper.EditorGUILayout.EnumDictionaryExcept<ConsoleExtraEnum.EDebugTypeBase, ConsoleExtraEnum.EDebugSpecailList>("User List", ref m_Consts.UserListFlag, lInfo1, true);

        m_Consts.ErrorList = Helper.EditorGUILayout.EnumDictionaryExcept<ConsoleExtraEnum.EDebugTypeBase, ConsoleExtraEnum.EDebugSpecailList>("Error List", ref m_Consts.ErrorListFlag, lInfo1, true);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("csc.exe", GUILayout.Width(40));
        Helper.EditorGUILayout.LabelFieldBlank(2);
        m_Logging.m_Consts.CSC_Directory = EditorGUILayout.TextField(m_Logging.m_Consts.CSC_Directory, GUILayout.Width(300));
        Helper.EditorGUILayout.LabelFieldBlank(5);
        if (GUILayout.Button("Find",  GUILayout.Width(50), GUILayout.Height(20)))
        {
            string lTemp = EditorUtility.OpenFilePanel("Find csc.file", m_Logging.m_Consts.CSC_Directory, "csc.exe");
            if(m_Logging.m_Consts.CSC_Directory != lTemp)
            {
                GUI.SetNextControlName(m_FakeControlID);
                m_Logging.m_Consts.CSC_Directory = lTemp;
                m_Logging.Save_CscFile();
                GUI.FocusControl(m_FakeControlID);
                Repaint();
            }            
        }
        Helper.EditorGUILayout.LabelFieldBlank(1);
        if (GUILayout.Button("Revert", GUILayout.Width(50), GUILayout.Height(20)))
        {
            GUI.SetNextControlName(m_FakeControlID);
            m_Logging.m_Consts.CSC_Directory = m_Logging.m_Consts.STAY_CSC_Directory;
            m_Logging.Save_CscFile();
            GUI.FocusControl(m_FakeControlID);
            Repaint();
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndScrollView();

        Helper.EditorGUILayout.Space(2);
        DisplaySaveButton();
    }


    /// <summary>
    /// 
    /// </summary>

    void DisplayList(string lName, ref List<string> lList)
    {
        GUIStyle lTextFieldStyle  = new GUIStyle(GUI.skin.textField);
        lTextFieldStyle.alignment = TextAnchor.MiddleLeft;

        GUIStyle lButtonStyle  = new GUIStyle(GUI.skin.button);
        lButtonStyle.alignment = TextAnchor.MiddleCenter;

        const int lNameWidth   = 120;

        Helper.EditorGUILayout.Space(3);

        for (int i = 0; i < lList.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            Helper.EditorGUILayout.LabelFieldBlank(lNameWidth);
            lList[i] = EditorGUILayout.TextArea(lList[i], lTextFieldStyle, GUILayout.Width(150), GUILayout.Height(20));
            Helper.EditorGUILayout.LabelFieldBlank(20);
            const char upArrow   = '\u25B2';
            const char downArrow = '\u25BC';
            const char cross     = '\u2718';



            Helper.EditorGUILayout.LabelFieldBlank(5);

            GUI.enabled = lList.CanMoveUp((uint)i);
            if (GUILayout.Button(upArrow.ToString(), lButtonStyle, GUILayout.Width(20), GUILayout.Height(20)))
            {
                GUI.SetNextControlName(m_FakeControlID);
                lList.MoveUp((uint)i);
                GUI.FocusControl(m_FakeControlID);
                Repaint();
            }


            Helper.EditorGUILayout.LabelFieldBlank(1);

            GUI.enabled = lList.CanMoveDown((uint)i);
            if (GUILayout.Button(downArrow.ToString(), lButtonStyle, GUILayout.Width(20), GUILayout.Height(20)))
            {
                GUI.SetNextControlName(m_FakeControlID);               
                lList.MoveDown((uint)i);
                GUI.FocusControl(m_FakeControlID);
                Repaint();
            }

            Helper.EditorGUILayout.LabelFieldBlank(2);
            GUI.enabled = true;
            Helper.EditorGUILayout.LabelFieldBlank(2);
            if (GUILayout.Button(cross.ToString(), lButtonStyle, GUILayout.Width(20), GUILayout.Height(20)))
            {
                GUI.SetNextControlName(m_FakeControlID);
                lList.RemoveAt(i);
                GUI.FocusControl(m_FakeControlID);
                Repaint();
            }


            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.BeginHorizontal();
        Helper.EditorGUILayout.LabelFieldBlank(lNameWidth);
        if (GUILayout.Button("+ Add", lButtonStyle, GUILayout.Width(lNameWidth), GUILayout.Height(20)))
        {
            GUI.SetNextControlName(m_FakeControlID);
            lList.Add("_TEMP_");
            GUI.FocusControl(m_FakeControlID);
            Repaint();
        }

        EditorGUILayout.EndHorizontal();
    }
    /// <summary>
    /// 
    /// </summary>
    void DisplaySaveButton()
    {
        Helper.EditorGUILayout.Space(3);
        EditorGUILayout.BeginHorizontal();
        int lMiddleWidth = Helper.ButtonSpace(m_Logging.m_Consts.ButtonStartWidth, m_Logging.m_Consts.ButtonWidth, 1, position.width, true);
        Helper.EditorGUILayout.LabelFieldBlank(lMiddleWidth);

        bool lSave = false;
        if (GUILayout.Button("Save", GUILayout.Width(m_Logging.m_Consts.ButtonWidth), GUILayout.Height(m_Logging.m_Consts.ButtonHeight)))
        {
            lSave = true;
        }
        EditorGUILayout.EndHorizontal();
        Helper.EditorGUILayout.Space(3);

        if (lSave == true)
        {
            bool lEarlyReturn = AreThereDuplicates();
            if (lEarlyReturn == true)
            {
                return;
            }

            bool lValidSave = Save();

            if (lValidSave == true)
            {
                EditorUtility.DisplayDialog("You have saved the new setting", " Please run the app again for Changes to take effect", "OK");

                this.Close();
                m_Settings.Close();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    bool AreThereDuplicates()
    {
        List<string> lAll = new List<string>(m_Consts.EDebugTypeBaseNameStrings);

        foreach (var lString in Enum.GetNames(typeof(ConsoleExtraEnum.EDebugSpecailList)).OfType<string>().ToList())
        {
            lAll.Add(lString);
        }
        List<string> lDuplicates = GetDuplicates(lAll);

        if (lDuplicates.Count != 0)
        {
            string lError = "";
            for (int i = 0; i < lDuplicates.Count; i++)
            {
                lError += lDuplicates[i] + "\n";
            }
            EditorUtility.DisplayDialog("There Are Duplicate,please check", lError, "OK");
        }

        return (lDuplicates.Count != 0);
    }

    /// <summary>
    /// 
    /// </summary>
    List<string> GetDuplicates(List<string> lList)
    {
        var duplicateKeys = lList.GroupBy(x => x)
                .Where(group => group.Count() > 1)
                .Select(group => group.Key);

        return duplicateKeys.ToList();
    }

}


