﻿using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour
{
    void Start()
    {
        ConsoleExtra.Log(this.gameObject.name, this.gameObject, ConsoleExtraEnum.EDebugType.Init);
    }

    private float m_CurrentTime = 0;
    public float m_Speed = 5;
    public float m_MaxTime   = 5;

    void Update()
    {
        m_CurrentTime += Time.deltaTime;
        if(m_CurrentTime > m_MaxTime)
        {
            ConsoleExtra.Log(this.gameObject.transform.rotation.eulerAngles.ToString(), this.gameObject, ConsoleExtraEnum.EDebugType.Rotation);
        }
        this.gameObject.transform.Rotate(Vector3.right, m_Speed * Time.deltaTime);
    }

    void OnCollisionEnter(Collision col)
    {
        ConsoleExtra.Log(this.gameObject.name, this.gameObject, ConsoleExtraEnum.EDebugType.Contact);
    }
}
