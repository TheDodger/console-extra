﻿using UnityEngine;
using System.Collections;

public class Capsule : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        ConsoleExtra.Log(this.gameObject.name, this.gameObject, ConsoleExtraEnum.EDebugType.Init);
    }
    public float m_destroy = 3;
    public float m_Speed = 10;
    // Update is called once per frame




    void Update ()
    {
        m_destroy -= Time.deltaTime;
        this.gameObject.transform.Rotate(Vector3.right, m_Speed * Time.deltaTime);

        if(m_destroy < 0)
        {
            ConsoleExtra.Log("destroyed " + this.gameObject.name, this.gameObject, ConsoleExtraEnum.EDebugType.Destroyed);
            Destroy(this.gameObject);
        }
    }
}
